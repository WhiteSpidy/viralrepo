//
//  CustomView.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 02/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit

extension UIView {
    
    func setUpViewForLoginDetails() {
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 1.0
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOpacity = 0.5
    }
    
}
