//
//  CustomTextField.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 02/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextFields

class CustomTextField: MDCBaseTextField {
    
    func setUpShadow() {
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 1.0
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.5
    }

    func rightSideImage(nameOfImage: String) {
        let image =  UIImageView(image: UIImage(named: nameOfImage))
        image.frame = CGRect(x: 0, y: 0, width: 16, height: 16)
        rightView = image
        rightViewMode = .always
    }
    
}

