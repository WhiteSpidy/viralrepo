//
//  SignUpScreen.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 02/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextFields
import Moya

class SignUpScreen: UIViewController {

//    MARK:- Declaring Outlets and Variables
    @IBOutlet weak var referalTableView: UITableView!
    @IBOutlet weak var imgViewProfie: UIImageView!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var txtBirthDate: CustomTextField!
    @IBOutlet weak var txtReferrer: CustomTextField!
    @IBOutlet weak var lblLogin: UILabel!
    
    var referalDummyData = [ReferalDetails]()
    var searchedReferals = [ReferalDetails]()
    var datePicker: UIDatePicker?
    
//    MARK:- View Did Load Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        setUpDatePicker()
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        
        guard let email = txtEmail.text else { return }
        guard let password = txtPassword.text else { return }
        guard let referrer = txtReferrer.text else { return }
        
        ApiType.signup(email: email, password: password, channelName: referrer, channelId: "Channel iD").request(type: UserDetails.self) { (user, error) in
            if error != nil {
                print(error?.statusCode as Any)
            } else {
                do {
                    print(user as Any)
                    let encoder = JSONEncoder()
                    let userData = try encoder.encode(user)
                    ConstantStrings.userDefaults.set(userData, forKey: "userDetails")
                    ConstantStrings.userDefaults.set(true, forKey: "isUserLoggedIn")
                    guard let storyBoard = UIStoryboard(name: "Pagers", bundle: .main) as? UIStoryboard else { return }
                    guard let pagerVC = storyBoard.instantiateViewController(withIdentifier: "PagerVC") as?  PagerVC else { return }
                    self.navigationController?.pushViewController(pagerVC, animated: true)
                } catch {
                    print("Cannot encode Data")
                }
            }
  
//        Alamofire
        
//        var parameters: [String: Any] = [:]
//        parameters["email"] = email
//        parameters["channelId"] = "channel id"
//        parameters["channelName"] = referrer
//        parameters["password"] = password
//        guard let url = URL(string: ApiHelper.urls.signupURL) else  { return }
        
//        ApiHelper.postSignup(url: url, parameters: parameters) { (response) in
//            switch response {
//            case .success(let data):
//                print(data)
//                break
//            case .failure(let error):
//                print(error)
//                break
//            }
//        }
    }
}
}

//MARK:- Extension for navigation controller delegate and image picker delegate
extension SignUpScreen: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    /// Setting image selected by user into our imageview
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imgViewProfie.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
    }
    
}
//MARK:- Extension for tableview delegate
extension SignUpScreen: UITableViewDelegate {
    
}

//MARK:- Extension for tableview datasource
extension SignUpScreen: UITableViewDataSource {
    
    /// Number of rows in tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchedReferals.count
    }
    
    /// Cell for row at
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReferalCell", for: indexPath) as? ReferalCell else { return UITableViewCell() }
        let data = searchedReferals[indexPath.row]
        cell.configureCell(referalDetails: data)
        cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    /// Did select row at
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtReferrer.text = searchedReferals[indexPath.row].name
        let image = UIImageView(image: searchedReferals[indexPath.row].image)
        txtReferrer.leadingView = image
        txtReferrer.leftViewMode = .always
        referalTableView.isHidden = true
    }
    
}
//MARK:- Extension for basic setup and functions
extension SignUpScreen {
    
    private func setUp() {
        /// Referal Targets
        txtReferrer.addTarget(self, action: #selector(searchText), for: .allEditingEvents)
        txtReferrer.addTarget(self, action: #selector(endEditing), for: .editingDidEnd)
        // Circular image View Profile Picture and tap gesture recognization
        /// Login Label
        let tap = UITapGestureRecognizer(target: self, action: #selector(loginTapped))
        lblLogin.isUserInteractionEnabled = true
        lblLogin.addGestureRecognizer(tap)
        imgViewProfie.layer.cornerRadius = imgViewProfie.frame.size.width/2
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        imgViewProfie.isUserInteractionEnabled = true
        imgViewProfie.addGestureRecognizer(tapGestureRecognizer)
        /// Setting right side icon of textFields and their hint
        txtEmail.rightSideImage(nameOfImage: "mail")
        txtBirthDate.rightSideImage(nameOfImage: "calender")
        txtReferrer.rightSideImage(nameOfImage: "search")
        txtEmail.label.text = "Email"
        txtPassword.label.text = "Password"
        txtBirthDate.label.text = "BirthDate"
        txtReferrer.label.text = "Refer"
        /// Updating Dummy data to referal array
        referalDummyData.append(ReferalDetails(name: "Hanif", imageName: "mail"))
        referalDummyData.append(ReferalDetails(name: "Test", imageName: "eyeOpen"))
        referalTableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        referalTableView.separatorStyle = .none
    }
    
    /// Setting Date Picker for Birthdate
    func setUpDatePicker() {
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.maximumDate = Calendar.current.date(byAdding: .year, value: -12, to: Date())
        txtBirthDate.inputView = datePicker
        //        Date picker toolbar
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle  = UIBarStyle.default
        let flexSpace  = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(datePickerDone))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        txtBirthDate.inputAccessoryView = doneToolbar
    }
    
    /// When User complete picking date
    @objc func datePickerDone() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        guard let selectedDate = datePicker?.date else { return }
        txtBirthDate.text = dateFormatter.string(from: selectedDate)
        self.view.endEditing(true)
    }
    
    @objc func loginTapped() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /// When User Ends Editing Referal
    @objc func endEditing() {
        if txtReferrer.text == "" {
            txtReferrer.leadingView = nil
        }
        referalTableView.isHidden = true
    }
    
    /// When User types on referal text tableview shows suggestion
    @objc func searchText() {
        guard let searchedText = txtReferrer.text else { return }
        searchedReferals = referalDummyData.filter({ (ReferalDetails) -> Bool in
        ReferalDetails.name.contains(searchedText)
            })
        if searchedReferals.count == 0 {
            referalTableView.isHidden = true
            return
        } else {
            referalTableView.isHidden = false
            referalTableView.reloadData()
        }
    }
    
    @objc func imageTapped() {
        let imageController = UIImagePickerController()
        imageController.delegate = self
        let alert = UIAlertController(title: "Select", message: "Choose a Source", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            imageController.sourceType = .camera
            self.present(imageController, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
           imageController.sourceType = .photoLibrary
            self.present(imageController, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension SignUpScreen: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let textField = textField as? CustomTextField else { return }
        textField.setUpShadow()
    }
}
