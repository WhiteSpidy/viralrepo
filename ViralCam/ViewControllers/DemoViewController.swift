//
//  DemoViewController.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 09/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit

class DemoViewController: UIViewController {

    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txttPassword: CustomTextField!
    @IBOutlet weak var txtBdate: CustomTextField!
    @IBOutlet weak var txtRefer: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtEmail.delegate = self
        txttPassword.delegate = self
        txtBdate.delegate = self
        txtRefer.delegate = self
    }
    
}

extension DemoViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtEmail {
            txtEmail.setUpShadow()
        } else if textField == txttPassword{
            txttPassword.setUpShadow()
        } else if textField == txtBdate{
            txtBdate.setUpShadow()
        } else {
            txtRefer.setUpShadow()
        }
    }
    
}
