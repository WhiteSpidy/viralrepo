//
//  ViewController.swift
//  Directions
//
//  Created by Mac on 9/2/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class appleMapVC: UIViewController {
    
    @IBOutlet var textFieldForAddress: UITextField!
    @IBOutlet var getDirectionsButton: UIButton!
    @IBOutlet weak var map: MKMapView!
    
    var locationManger = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestAlwaysAuthorization()
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        
        map.delegate = self
        
    }
    
    @IBAction func getDirectionsTapped(_ sender: Any) {
        getAddress()
    }
    
}

extension appleMapVC: CLLocationManagerDelegate {
    
}


extension appleMapVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        render.strokeColor = .blue
        return render
    }
    
}

extension appleMapVC {
    
    func getAddress() {
        let geoCoder = CLGeocoder()
        guard let searchedText = textFieldForAddress.text else { return }
        geoCoder.geocodeAddressString(searchedText) { (placemarks, error) in
            guard let placemarks = placemarks, let location = placemarks.first?.location
                else {
                    print("Location Not Found")
                    return
            }
            print(location)
            self.mapThis(destinationCord: location.coordinate)
        }
    }
    
    func mapThis(destinationCord : CLLocationCoordinate2D) {
//        Source Loaction
        guard let souceCordinate = (locationManger.location?.coordinate) else { return }
//        Annotations
        let sourceAnnotation = MKPointAnnotation()
        let destinationAnnotation = MKPointAnnotation()
        sourceAnnotation.coordinate = souceCordinate
        map.addAnnotation(sourceAnnotation)
        destinationAnnotation.coordinate = destinationCord
        map.addAnnotation(destinationAnnotation)
//        Placemarks
        let soucePlaceMark = MKPlacemark(coordinate: souceCordinate)
        let destPlaceMark = MKPlacemark(coordinate: destinationCord)
//        Items
        let sourceItem = MKMapItem(placemark: soucePlaceMark)
        let destItem = MKMapItem(placemark: destPlaceMark)
//        Request
        let destinationRequest = MKDirections.Request()
        destinationRequest.source = sourceItem
        destinationRequest.destination = destItem
        destinationRequest.transportType = .walking
        let directions = MKDirections(request: destinationRequest)
        directions.calculate { (response, error) in
            guard let response = response else {
                if let error = error {
                    print(error.localizedDescription)
                }
                return
            }
            self.map.removeOverlays(self.map.overlays)
            let route = response.routes[0]
            self.map.addOverlay(route.polyline)
            self.map.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
        }
    }
}
