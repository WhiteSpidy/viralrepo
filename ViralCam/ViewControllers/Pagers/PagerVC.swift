//
//  PagerVC.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 08/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit
import Pageboy

class PagerVC: PageboyViewController {

    lazy var viewControllers: [UIViewController] = {
        var viewControllers = [UIViewController]()
        guard let storyBoard = UIStoryboard(name: "Pagers", bundle: .main) as? UIStoryboard else { return [UIViewController()] }
        viewControllers.append(storyBoard.instantiateViewController(identifier: "Page1VC"))
        viewControllers.append(storyBoard.instantiateViewController(identifier: "Page2VC"))
        viewControllers.append(storyBoard.instantiateViewController(identifier: "Page3VC"))
        return viewControllers
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.isInfiniteScrollEnabled = true
    }
    
}

extension PagerVC: PageboyViewControllerDataSource {
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return viewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        return viewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
    
}
