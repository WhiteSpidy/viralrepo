//
//  HomeVC.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 05/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit
import CarbonKit

class Page1VC: UIViewController {

    var controllers = ["Following","Discover","My Upvotes"]
    var carbontabSwipeNavigation = CarbonTabSwipeNavigation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }

}

extension Page1VC: CarbonTabSwipeNavigationDelegate {
   
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        guard let storyBoard = UIStoryboard(name: "Main", bundle: .main) as? UIStoryboard else { return UIViewController() }
        switch index {
        case 0:
            return storyBoard.instantiateViewController(withIdentifier: "VC1")
        case 1:
            return storyBoard.instantiateViewController(withIdentifier: "VC2")
        default:
            return storyBoard.instantiateViewController(withIdentifier: "VC3")
        }
    }
    
}

extension Page1VC {
    
    func setUp() {
        carbontabSwipeNavigation = CarbonTabSwipeNavigation(items: controllers, delegate: self)
        carbontabSwipeNavigation.insert(intoRootViewController: self)
        carbontabSwipeNavigation.setIndicatorColor(UIColor.white)
        carbontabSwipeNavigation.setSelectedColor(UIColor.white)
        carbontabSwipeNavigation.setNormalColor(UIColor.white)
        carbontabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor(named: "ButtonColor")
        carbontabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width/3, forSegmentAt: 0)
        carbontabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width/3, forSegmentAt: 1)
        carbontabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width/3, forSegmentAt: 2)
    }

}
