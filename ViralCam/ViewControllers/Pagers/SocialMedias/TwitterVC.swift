//
//  TwitterVC.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 05/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit

class TwitterVC: UIViewController {
    
    @IBOutlet weak var tblViewList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblViewList.register(UINib(nibName: "GameModeCell", bundle: nil), forCellReuseIdentifier: "GameModeCell")
    }
    
}

extension TwitterVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    
}

extension TwitterVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GameModeCell", for: indexPath) as? GameModeCell else { return UITableViewCell() }
        return cell
    }
    
    
    
}
