//
//  ViewController.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 02/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextControls_FilledTextFields
import Moya
import ObjectMapper

class LoginScreen: UIViewController {

    @IBOutlet weak var txtEmail: MDCFilledTextField!
    @IBOutlet weak var txtPassword: MDCFilledTextField!
    @IBOutlet weak var btnTogglePassword: UIButton!
    @IBOutlet weak var lblSignUp: UILabel!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewEmail: UIView!
    var isPasswordVisible = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        hideKeyboardWhenTappedAround()
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        guard let email = txtEmail.text else { return }
        guard let password = txtPassword.text else { return }
        
        ApiType.login(username: email, password: password).request(type: UserDetails.self) { (user, error) in
            if error != nil {
                print(error?.statusCode as Any)
            } else {
                do {
                    print(user as Any)
                    let encoder = JSONEncoder()
                    let userData = try encoder.encode(user)
                    ConstantStrings.userDefaults.set(userData, forKey: "userDetails")
                    ConstantStrings.userDefaults.set(true, forKey: "isUserLoggedIn")
                    guard let storyBoard = UIStoryboard(name: "Pagers", bundle: .main) as? UIStoryboard else { return }
                    guard let pagerVC = storyBoard.instantiateViewController(withIdentifier: "PagerVC") as?  PagerVC else { return }
                    self.navigationController?.pushViewController(pagerVC, animated: true)
                } catch {
                    print("Cannot encode Data")
                }
            }
        }
        
//        ALamofire Api Call
        
//        guard let email = txtEmail.text else { return }
//        guard let password = txtPassword.text else { return }
//        var parameters: [String: Any] = [:]
//        parameters["username"] = email
//        parameters["password"] = password
//        guard let url = URL(string: ApiHelper.urls.loginURL) else  { return }
//
//        ApiHelper.postLogin(url: url, parameters: parameters) { (response) in
//            switch response {
//            case .success(let data):
//                print(data)
//                break
//            case .failure(let error):
//                print(error)
//                break
//            }
//        }
        
    }
    
    @IBAction func btnTogglePassword(_ sender: Any) {
        if isPasswordVisible {
            txtPassword.isSecureTextEntry = false
            btnTogglePassword.setImage(UIImage(named: "eyeOpen"), for: .normal)
            isPasswordVisible = !isPasswordVisible
        } else {
            txtPassword.isSecureTextEntry = true
            btnTogglePassword.setImage(UIImage(named: "eyeClosed"), for: .normal)
            isPasswordVisible = !isPasswordVisible
        }
    }
    
}

extension LoginScreen {
    
    private func setUp() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(signUpTapped))
        lblSignUp.isUserInteractionEnabled = true
        lblSignUp.addGestureRecognizer(tap)
        viewEmail.setUpViewForLoginDetails()
        viewPassword.setUpViewForLoginDetails()
        txtEmail.label.text = "Email"
        txtPassword.label.text = "Password"
        txtEmail.rightView = rightSideImage(nameOfImage: "mail")
        txtEmail.rightViewMode = .always
        txtPassword.rightViewMode = .always
    }
    
    func rightSideImage(nameOfImage: String)-> UIView {
        let Image =  UIImageView(image: UIImage(named: nameOfImage))
        Image.frame = CGRect(x: 0, y: 0, width: 16, height: 16)
        return Image
    }
    
    @objc func signUpTapped() {
        guard let signUpScreen = storyboard?.instantiateViewController(identifier: "SignUpScreen") as? SignUpScreen else { return }
        self.navigationController?.pushViewController(signUpScreen, animated: true)
    }
    
}

extension LoginScreen:UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case txtEmail:
            viewEmail.layer.shadowColor = UIColor.gray.cgColor
        default:
            viewPassword.layer.shadowColor = UIColor.gray.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtEmail:
            viewEmail.layer.shadowColor = UIColor.white.cgColor
        default:
            viewPassword.layer.shadowColor = UIColor.white.cgColor
        }
    }
    
}
