//
//  mapsViewController.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 15/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class mapsViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var sourceLat: Double? = nil
    var sourceLong: Double? = nil
    var sourceLocation: String? = nil
    var destinationLocation: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
    }
    
}

extension mapsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue = manager.location?.coordinate else { return }
        sourceLat = locValue.latitude
        sourceLong = locValue.longitude
        sourceLocation = "\(locValue.latitude),\(locValue.longitude)"
        showMarker()
    }
    
}

extension mapsViewController {
    
    func showMarker() {
//        let destinationLocation = "20.416234,72.831505"
//        let stringURL = ApiHelper.getURL(sourceLoc: sourceLocation!, destinationLoc: destinationLocation)
//        let url = URL(fileURLWithPath: stringURL)
//        getApiData(source: sourceLocation!, destination: destinationLocation)
//        
//        let sourceMarker = GMSMarker()
//        sourceMarker.position = CLLocationCoordinate2D(latitude: sourceLat!, longitude: sourceLong!)
//        sourceMarker.title = "Source"
//        sourceMarker.map = self.mapView
//        
//        // MARK: Marker for destination location
//        let destinationMarker = GMSMarker()
//        destinationMarker.position = CLLocationCoordinate2D(latitude: 20.416234, longitude: 72.831505)
//        destinationMarker.title = "Destination"
//        destinationMarker.map = self.mapView
//        
//        let camera = GMSCameraPosition(target: destinationMarker.position, zoom: 10)
//        self.mapView.animate(to: camera)
        
    }
    
    func getApiData(source: String, destination: String) {
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(destination)&sensor=true&mode=driving&key=AIzaSyAbrQTxfoxeIxvNU6U2-Og81DUepJs4FOY")!
        
        AF.request(url).responseJSON { (reseponse) in
            guard let data = reseponse.data else {
                print(reseponse.error?.errorDescription)
                return
            }
            do {
                let jsonData = try JSON(data: data)
                let routes = jsonData["routes"].arrayValue
                for route in routes {
                    let overview_polyline = route["overview_polyline"].dictionary
                    let points = overview_polyline?["points"]?.string
                    let path = GMSPath.init(fromEncodedPath: points ?? "")
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeColor = .systemBlue
                    polyline.strokeWidth = 5
                    polyline.map = self.mapView
                }
            }
             catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
}
