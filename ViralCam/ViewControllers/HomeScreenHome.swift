//
//  HomeScreenHome.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 05/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit
import CarbonKit

class HomeScreenHome: UIViewController {

    var controllers = ["Twitter","Instagram","SnapChat", "Youtube"]
    var carbbTop = ["Testing","Demo","New"]
    var carbontabSwipeNavigation = CarbonTabSwipeNavigation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }

}

extension HomeScreenHome: CarbonTabSwipeNavigationDelegate {
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        guard let storyBoard = storyboard else { return UIViewController() }
        
        switch index {
        case 0:
            return storyBoard.instantiateViewController(withIdentifier: "TwitterVC")
        case 1:
            return storyBoard.instantiateViewController(withIdentifier: "FacebookVC")
        case 2:
            return storyBoard.instantiateViewController(withIdentifier: "InstagramVC")
        default:
            return storyBoard.instantiateViewController(withIdentifier: "SnapChatVC")
        }
    }
    
}

extension HomeScreenHome {
    
    func setUp() {
        carbontabSwipeNavigation = CarbonTabSwipeNavigation(items: carbbTop, delegate: self)
        carbontabSwipeNavigation = CarbonTabSwipeNavigation(items: controllers, delegate: self)
        carbontabSwipeNavigation.insert(intoRootViewController: self)
        carbontabSwipeNavigation.setIndicatorColor(UIColor.white)
        carbontabSwipeNavigation.setSelectedColor(UIColor.white)
        carbontabSwipeNavigation.setNormalColor(UIColor.white)
        carbontabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor(named: "ButtonColor")
        carbontabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width/4, forSegmentAt: 0)
        carbontabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width/4, forSegmentAt: 1)
        carbontabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width/4, forSegmentAt: 2)
        carbontabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width/4, forSegmentAt: 3)
    }
    
}
