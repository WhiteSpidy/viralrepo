//
//  ApiHelper.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 15/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit
import Moya_ObjectMapper
import ObjectMapper
import Moya
import Alamofire

class ApiHelper {
    
    static let baseURL = "https://stage-api.austinconversionoptimization.com/api"
    
    struct urls {
        static let loginURL = "\(baseURL)/auth/signin"
        static let signupURL = "\(baseURL)/auth/signupEmail"
    }
    
    class func postLogin(url: URL, parameters: [String: Any], completion: @escaping (Swift.Result<[String: Any], Error>) -> Void) {
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch  response.result {
            case .success(let data):
                guard let result = data as? [String: Any] else {return}
                completion(.success(result))
                print(result)
                break
            case .failure(let error):
                completion(.failure(error))
                print(error)
            }
        }
    }
    
    class func postSignup(url: URL, parameters: [String: Any], completion: @escaping (Swift.Result<[String: Any], Error>) -> Void) {
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch  response.result {
            case .success(let data):
                guard let result = data as? [String: Any] else {return}
                completion(.success(result))
                print(result)
                break
            case .failure(let error):
                completion(.failure(error))
                print(error)
            }
        }
    }
    
}

enum ApiType {
    case login(username: String, password: String)
    case signup(email: String, password: String, channelName: String, channelId: String)
}

extension ApiType: TargetType {
    
    var path: String {
        switch self {
        case .login:
            return "/auth/signin"
        case .signup:
            return "/auth/signupEmail"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login, .signup:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var parameters: [String: Any] {
        switch self {
        case .login(let username, let password):
            var parameters = [String: Any]()
            parameters["username"] = username
            parameters["password"] = password
            return parameters
        case .signup(let email, let password, let channelName, let channelId):
            var parameters = [String: Any]()
            parameters["email"] = email
            parameters["password"] = password
            parameters["channelName"] = channelName
            parameters["channelId"] = channelId
            return parameters
        }
    }
    
    var task: Task {
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    var baseURL: URL {
        return URL(string: "https://stage-api.austinconversionoptimization.com/api")!
    }
    
}

extension ApiType {
    
    func request<T: Mappable>(type: T.Type, completion: @escaping (_ response: T?, _ error: APIError?) -> Void) {
        let provider = MoyaProvider<ApiType>()
        provider.request(self) { (result) in
            switch result {
            case .success(let response):
                if let mappableObject = try? response.mapObject(type) {
                    completion(mappableObject, nil)
                } else {
                    completion(nil, APIError(description: "Cannot Fetch Data", statusCode: 404))
                }
            case .failure(let error):
                completion(nil, APIError(description: "", statusCode: error.response?.statusCode ?? -1))
            }
        }
    }
    
}

struct APIError {
    var description: String
    var statusCode: Int
}
