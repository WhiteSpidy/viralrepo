
//
//  DemoVC.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 12/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialBottomAppBar

class DemoVCForBottomTabBar: UIViewController {
    
    @IBOutlet weak var btnCenter: UIButton!
    @IBOutlet weak var bottomAppBar: MDCBottomAppBarView!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCenter.layer.cornerRadius = 0.5 * btnCenter.bounds.size.width;
        addBottomBar()
        
    }
    
    
}

extension DemoVCForBottomTabBar {
    
    
    @objc func onMenuButtonTapped() {
        print("On Menu Tapped")
    }
    
    @objc func onNavigationButtonTapped() {
        print("On Nav tapped")
    }
    
    
    func addBottomBar() {
            bottomAppBar.barTintColor = UIColor(named: "WarmGrey")
            // Image floatingButton
            bottomAppBar.floatingButton.setImage(UIImage(named: "tabCenter"), for: .normal)
            // Background color floatingButton
            bottomAppBar.floatingButton.backgroundColor = UIColor(named: "ButtonColor")
            
            // The following lines of code are to define the buttons on the right and left side
            let firstTab = UIBarButtonItem(
                image: UIImage(named:"HomeTab"), // Icon
                style: .plain,
                target: self,
                action: #selector(self.onMenuButtonTapped))
        firstTab.accessibilityLabel = "Home"
        firstTab.accessibilityHint = "Home"
//            firstTab.customView?.frame = CGRect(x: 0,y: 0,width: 150,height: 50)
            
//            let secondTab = UIBarButtonItem(
//                image: UIImage(named:"Content"), // Icon
//                style: .plain,
//                target: self,
//                action: #selector(self.onMenuButtonTapped))
//            secondTab.customView?.frame = CGRect(x: 0,y: 0,width: 50,height: 50)
//
//            let thirdTab = UIBarButtonItem(
//                image: UIImage(named:"NotificationTab"), // Icon
//                style: .plain,
//                target: self,
//                action: #selector(self.onMenuButtonTapped))
//            thirdTab.customView?.frame = CGRect(x: 0,y: 0,width: 50,height: 50)
            
            
            let fourthTab = UIBarButtonItem(
                image: UIImage(named: "ProfileTab"), // Icon
                style: .plain,
                target: self,
                action: #selector(self.onNavigationButtonTapped))
//            fourthTab.customView?.frame = CGRect(x: 0,y: 0,width: 50,height: 50)
            
            bottomAppBar.leadingBarButtonItems = [firstTab]
            bottomAppBar.trailingBarButtonItems = [fourthTab]
            
    }
    
}
