//
//  ReferalCell.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 03/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import UIKit

class ReferalCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureCell(referalDetails: ReferalDetails) {
        lblName.text = referalDetails.name
        profileImage.image = referalDetails.image
    }
    
}
