//
//  UserProfile.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 17/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import Foundation
import ObjectMapper

class UserDetails: Mappable, Codable {
    
    var userToken: String?
    var userName: String?
    var email: String?
    var displayName: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        userToken <- map["token"]
        userName <- map["username"]
        email <- map["email"]
        displayName <- map["displayName"]
    }
    
}

