//
//  ReferalData.swift
//  ViralCam
//
//  Created by Mohammed Hanifbiji on 03/06/20.
//  Copyright © 2020 com.Simform. All rights reserved.
//

import Foundation
import UIKit

class ReferalDetails {
    var name: String
    var image: UIImage?
    
    init(name: String, imageName: String) {
        self.name = name
        self.image = UIImage(named: imageName)
    }
}
